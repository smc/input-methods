This is a description about Poorna Extended Remington Typewriter Keyboard Layout mapping for m17n used in Gnu/Linux systems to input Unicode Malayalam characters. The mapping file was authored by Mujeeb Rahman K <mujeebcpy@gmail.com> of Swathanthra Malayalam Computing <http://smc.org.in>, an advocacy group for localisation and promotion of free sofware. The icon file was created by Hiran Venugopal <hiran.v@gmail.com> of SMC. This file should be maintained in UTF-8 encoding.

History of Remington Layout

Initially, there were two typewriters that were used to type Malayalam Language text, one among them being Remington. Typewriter Malayalam had much limitation as there were no way to include all of the complex characters in available keys.  So "koottaksharams" were typed using a virama (chandrakkala) in between. Later, when computers began to be deployed, Ascii fonts were hacked to display Malayalam glyphs. So the remington keyboard layout for inputting Malayalam was ported to the external input method used in computers. As there were many players working in these area, the computer adaptation of the keyboard varied much. Though the base keys were the same, Koottaksharams were mapped in different locations. The way short vowels were made long vowels were different in different adaptations. This keyboard layout that you use now has been mapped after studying most of the major adaptations and converging it with my own logic. 

Major changes and features

included all features of ml-remington layout, and included all Malayalam Unicode characters and punctuations.
read more on https://poorna.smc.org.in/

Installation instructions

In short: 
   
Download ml-poorna-remington.mim
Keep it in /usr/share/m17n
Add the layout to your Ibus preferences.
Use the keyboard.

In detail: 
For Arch Linux users

first install yaourt from AUR

   $ sudo pacman -S yaourt


Then install ibus and related packages using yaourt. Yaourt will take care of packaging the source files for you.

   $ yaourt -S ibus ibus-table ibus-table-extraphrase ibus-m17n ibus-qt ibus-table-others ibus-table-quick


(NB: Some of these packages may not be needed. )



Common Steps:

Download ml-poorna-remington.mim file.

Assuming that, you have downloaded the same to ~/Downloads folder, Go to Terminal and type as following. (The part after $ sign)

   your-username ~ $ cd Downloads
   your-username ~ $ sudo mv ml-poorna-remington.mim /usr/share/m17n/


It will ask for password. Give it.

Now, edit your .bashrc file to include the below written lines. (Not necessary in Ubuntu). Append it to the end of the file.

   your-username ~ $ nano .bashrc


(Remember, there is a dot before bashrc as the file is hidden. To copy and paste, just select the lines written below, go to the file, and click on the middle mouse button.)

     export GTK_IM_MODULE=ibus
     export XMODIFIERS=@im=ibus
     export QT_IM_MODULE=ibus


Use Ctrl+X to exit nano. It will ask whether to save. Give Y.

Now logout and re-login. If everything goes Okay, your keyboard is ready to use.

Now, access Ibus preferences from

   System >> Preferences >> Ibus Preferences (in Ubuntu.)


or

   K >>  Applications >> Settings >> Ibus Preferences (in Arch linux / KDE)


Select the tab, <Input Method>.

There is a drop down menu <Select an input method>

Click on it and find remington (m17n) under Malayalam. Click on Add button. As you have placed three lines in your .bashrc, the ibus would automatically be invoked at startup. So better add ispell (m17n) under English too and keep it as default. The first entry would be default. You could move it up and down using the arrow keys.

(If you are in KDE, remember to switch off xkb layout by going to

K >> System Settings >> Hardware >> Input Devices >> Layouts >> Layout Indicator

and uncheck both <Show layout indicator> and <Configure Layouts>.

Now you could use Cntrl + Space to turn Ibus off or on. Also, you could use Ctrl + Shift to switch between different keyboard layouts. You could use as many layouts as you please. 

